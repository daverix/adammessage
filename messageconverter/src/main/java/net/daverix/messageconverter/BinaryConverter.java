package net.daverix.messageconverter;

public class BinaryConverter {
    public String fromBinary(String message) {
        final StringBuilder builder = new StringBuilder();
        int count = 0;
        do {
            int length = Math.min(count+8, message.length());
            int binary = Integer.parseInt(message.substring(count, length));
            int decimal = getDecimalFromBinary(binary);
            builder.append((char)decimal);
            count += 8;
        } while (count < message.length());

        return builder.toString();
    }

    private int getDecimalFromBinary(int binary){
        int decimal = 0;
        int power = 0;
        while(true){
            if(binary == 0){
                break;
            } else {
                decimal += (binary % 10) * Math.pow(2, power);
                binary = binary / 10;
                power++;
            }
        }
        return decimal;
    }

    public String toBinary(byte[] bytes) {
        StringBuilder binary = new StringBuilder();
        for (byte b : bytes)
        {
            int val = b;
            for (int i = 0; i < 8; i++)
            {
                binary.append((val & 128) == 0 ? 0 : 1);
                val <<= 1;
            }
        }
        return binary.toString();
    }
}
