package net.daverix.messageconverter;

import javax.xml.bind.DatatypeConverter;

/**
 * Created by david.laurell on 2014-08-11.
 */
public class Base64Converter {
    public byte[] decodeBase64(String message) {
        return DatatypeConverter.parseBase64Binary(message);
    }

    public String encodeBase64(byte[] message) {
        return DatatypeConverter.printBase64Binary(message);
    }
}
