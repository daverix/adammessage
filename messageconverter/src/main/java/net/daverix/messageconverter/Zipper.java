package net.daverix.messageconverter;

import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.DeflaterInputStream;
import java.util.zip.InflaterOutputStream;

public class Zipper {
    public byte[] unzip(byte[] data) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        InflaterOutputStream stream = new InflaterOutputStream(out);
        stream.write(data);
        stream.close();
        try {
            return out.toByteArray();
        } finally {
            out.close();
        }
    }

    public byte[] zip(byte[] data) throws IOException {
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        DeflaterInputStream stream = new DeflaterInputStream(in);
        try {
            return IOUtils.toByteArray(stream);
        } finally {
            stream.close();
            in.close();
        }
    }
}
