package net.daverix.messageconverter;

import java.io.IOException;

public class MessageConverter {
    private final BinaryConverter mBinaryConverter;
    private final Base64Converter mBase64Converter;
    private final Zipper mZipper;

    public MessageConverter() {
        mBinaryConverter = new BinaryConverter();
        mBase64Converter = new Base64Converter();
        mZipper = new Zipper();
    }

    public String to(String message) throws IOException {
        final byte[] zipped = mZipper.zip(message.getBytes());
        final String base64encoded = mBase64Converter.encodeBase64(zipped);
        return mBinaryConverter.toBinary(base64encoded.getBytes());
    }

    public String from(String message) throws IOException {
        final String asciiFromBinary = mBinaryConverter.fromBinary(message);
        final byte[] base64decoded = mBase64Converter.decodeBase64(asciiFromBinary);
        final byte[] unzipped = mZipper.unzip(base64decoded);

        return new String(unzipped, "UTF-8");
    }
}
