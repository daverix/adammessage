package net.daverix.messageconverter;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class MessageConverterTest {
    private MessageConverter mSut;

    @Test
    public void shouldConvertMessageToAndFrom() throws Exception {
        final String expected = "hej123";
        final String converted = mSut.to(expected);
        final String actual = mSut.from(converted);

        assertThat(actual, is(equalTo(expected)));
    }

    @Test
    public void shouldConvertAdamsMessage() throws Exception {
        final String expected = "something";
        final String actual = mSut.from("01100101010010100111100001111010011110100100110101101100010100100111000101001101011101110111011001001100010101100100101001001001010100110110100101111000010011110101011001010101011001110111001101010011011011000101011001001001010100110111001100110011010010100111101000110000011101000101100001001011010011010110110001011000011110010100010100110000010001100100000101001011010101010111100101000011011100010110011100111101");

        assertThat(actual, is(equalTo(expected)));
    }

    @Test
    public void shouldConvertMessageToAdam() throws Exception {
        final String expected = "something";
        final String actual = mSut.to("Jag har lagt alldelles för mycket tid på att lösa adams meddelande! :)");

        assertThat(actual, is(equalTo(expected)));
    }

    @Before
    public void setUp() {
        mSut = new MessageConverter();
    }
}
