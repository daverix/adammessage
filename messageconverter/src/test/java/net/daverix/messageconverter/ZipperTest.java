package net.daverix.messageconverter;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class ZipperTest {
    private Zipper mSut;

    @Before
    public void setUp() throws Exception {
        mSut = new Zipper();
    }

    @Test
    public void testZipAndUnzip() throws Exception {
        String expected = "hej123";
        byte[] compressed = mSut.zip(expected.getBytes());
        String actual = new String(mSut.unzip(compressed));
        assertThat(actual, is(equalTo(expected)));
    }
}