package net.daverix.messageconverter;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class BinaryConverterTest {
    private BinaryConverter mSut;

    @Before
    public void setUp() throws Exception {
        mSut = new BinaryConverter();
    }

    @Test
    public void testFromBinary() throws Exception {
        final String expected = "hej du glade";
        final String actual = mSut.fromBinary("011010000110010101101010001000000110010001110101001000000110011101101100011000010110010001100101");

        assertThat(actual, is(equalTo(expected)));
    }

    @Test
    public void testToBinary() throws Exception {
        final String expected = "011010000110010101101010001000000110010001110101001000000110011101101100011000010110010001100101";
        final String actual = mSut.toBinary("hej du glade".getBytes());

        assertThat(actual, is(equalTo(expected)));
    }
}